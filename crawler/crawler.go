package main

type ChanCrawler struct{}

var totalAddedPosts = 0

func (c *ChanCrawler) crawl(boards []string) Error {
	mkdir(dataDir)

	for _, board := range boards {
		mkdir(dataDir + board)
		initFile(dataDir+board+"/post_ids.json", "[]")
		initFile(dataDir+board+"/data.csv", "Timestamp,Post ID,Thread,Name,Title,Body")

		b := BoardCrawler{}
		b.crawl(board)

		logger.Info("Writing csv file for " + board)

		ids := loadFileJSON(dataDir + board + "/post_ids.json")
		posts := make([][]string, 0)

		for _, post := range b.Posts {
			id := int2String(post.ID)

			if !inSlice(id, ids) && post.Body != "" {
				posts = append(posts, []string{
					int2String(post.Time),
					id,
					post.Thread,
					post.Name,
					post.Title,
					post.Body,
				})
			}
		}

		writeFileJSON(dataDir+board+"/post_ids.json", b.PostIDs)

		csv := encodeCSVSlice(posts)
		appendToFile(dataDir+board+"/data.csv", "\n"+csv)

		logger.Info("Saved " + nint2String(len(posts)) + " posts from " + board)
		totalAddedPosts += len(posts)
	}

	return nil
}
