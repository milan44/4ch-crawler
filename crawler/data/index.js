function fillTable(data) {
    var totalDirSize = 0,
        totalPostCount = 0;

    var table = document.getElementsByTagName("table")[0];
    for (var key in data) {
        if (!data.hasOwnProperty(key)) continue;
    
        var value = data[key];
        var label = key.replace(/\\/gm, "/").split("/");
        label.shift();
        label = label.join("/");

        totalDirSize += parseInt(value.DirSize);
        totalPostCount += parseInt(value.PostCount);

        table.innerHTML += '<tr><th class="label"><a href="' + label + '" download>' + label + '</a></th><td class="content">' +
            '<table class="sub">' +
            '<tr><th>Size</th><td>' + formatBytes(value.DirSize) + '</td></tr>' +
            '<tr><th>Posts</th><td>' + numberWithCommas(value.PostCount) + '</td></tr>' +
            '<tr><th>Since</th><td>' + (value.FirstCrawl ? fmtDate(value.FirstCrawl) : 'N/A') + '</td></tr>' +
            '</table></td></tr>';
    }

    document.getElementById('total').innerHTML = '<div class="content">' +
        '<table class="sub">' +
        '<tr><th>Size</th><td>' + formatBytes(totalDirSize) + '</td></tr>' +
        '<tr><th>Posts</th><td>' + numberWithCommas(totalPostCount) + '</td></tr>' +
        '</table></div>';
}
function fmtDate(stamp) {
	stamp = parseInt(stamp);
	
	if (stamp) {
		var d = new Date(stamp * 1000);
		
		return padDate(d.getUTCDate()) + '.' + padDate(d.getUTCMonth()+1) + '.' + padDate(d.getUTCFullYear()) + ' ' + padDate(d.getUTCHours()) + ':' + padDate(d.getUTCMinutes()) + ' UTC';
	} else {
		return 'N/A';
	}
}
function padDate(d) {
	return d < 10 ? '0' + d : d;
}

function formatBytes(bytes) {
    var decimals = 2;
    if (bytes === 0) return '0 Bytes';

    var k = 1024;
    var dm = decimals < 0 ? 0 : decimals;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    var i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        fillTable(JSON.parse(xhttp.responseText));
    }
};
xhttp.open("GET", "index.json", true);
xhttp.send();