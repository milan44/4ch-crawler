package main

import (
	"fmt"
	"runtime"
	"strconv"
	"strings"
)

type Error interface {
	Error() string
	Trace() string

	GetError() error
}

type TraceInformation struct {
	File  string
	Line  int
	Name  string
	Entry uintptr
}

type CrawlerError struct {
	OriginalError error
	BackTrace     []TraceInformation
}

func NewError(err error) Error {
	if err == nil {
		return nil
	}

	c := CrawlerError{
		OriginalError: err,
		BackTrace:     make([]TraceInformation, 0),
	}

	i := 1
	for {
		pc, file, line, ok := runtime.Caller(i)
		if !ok {
			break
		}
		ti := TraceInformation{
			File: file,
			Line: line,
		}

		details := runtime.FuncForPC(pc)
		ti.Name = details.Name()
		ti.Entry = details.Entry()

		c.BackTrace = append(c.BackTrace, ti)

		i++
	}

	return c
}

func (c CrawlerError) Error() string {
	return c.OriginalError.Error()
}

func (c CrawlerError) GetError() error {
	return c.OriginalError
}

func (c CrawlerError) Trace() string {
	str := make([]string, len(c.BackTrace))

	for i, trace := range c.BackTrace {
		str[i] = trace.Name + "() [0x" + fmt.Sprintf("%x", trace.Entry) + "]\n\t" + trace.File + ":" + strconv.Itoa(trace.Line)
	}

	return strings.Join(str, "\n")
}
