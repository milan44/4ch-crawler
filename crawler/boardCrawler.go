package main

import (
	"sync"
)

type Post struct {
	Time   int64
	ID     int64
	Thread string
	Name   string
	Title  string
	Body   string
}

type BoardCrawler struct {
	Board     string
	ThreadIDs []string
	Posts     []Post
	PostIDs   []string

	Mutex sync.Mutex
}

func (c *BoardCrawler) crawl(board string) Error {
	err := c.loadBoard(board)
	if err != nil {
		return err
	}

	return c.loadPosts()
}

func (c *BoardCrawler) loadBoard(board string) Error {
	logger.Info("Loading Threads from " + board)
	c.Board = board

	url := "http://a.4cdn.org/" + board + "/catalog.json"

	json, err := loadJSONArray(url)
	if err != nil {
		return err
	}

	c.ThreadIDs = make([]string, 0)
	for _, page := range json {
		threads := page["threads"].([]interface{})

		for _, rawThread := range threads {
			thread := rawThread.(map[string]interface{})

			c.ThreadIDs = append(c.ThreadIDs, float2String(i2Float(thread["no"])))
		}
	}

	logger.Info("Found " + nint2String(len(c.ThreadIDs)) + " threads on " + board)

	return nil
}

func (c *BoardCrawler) loadPosts() Error {
	logger.Info("Loading Posts from " + c.Board)

	c.Posts = make([]Post, 0)
	c.PostIDs = make([]string, 0)

	var wg sync.WaitGroup
	wg.Add(len(c.ThreadIDs))

	for _, threadID := range c.ThreadIDs {
		go func(wg *sync.WaitGroup, board string, threadID string) {
			url := "https://a.4cdn.org/" + board + "/thread/" + threadID + ".json"

			json, err := loadJSONMap(url, 0)
			if err != nil {
				if err.Error() == "skip" {
					wg.Done()
					return
				}
				mailPanic(err)
			}

			posts := json["posts"].([]interface{})
			for i, rawPost := range posts {
				post := rawPost.(map[string]interface{})

				thisThreadID := "0"
				title := "0"
				if i > 0 {
					thisThreadID = threadID
				} else {
					title = i2String(post["sub"])
				}

				p := Post{
					Time:   float2Int(i2Float(post["time"])),
					ID:     float2Int(i2Float(post["no"])),
					Name:   i2String(post["name"]),
					Title:  title,
					Body:   normalizeBody(i2String(post["com"])),
					Thread: thisThreadID,
				}

				c.Mutex.Lock()
				c.Posts = append(c.Posts, p)
				c.PostIDs = append(c.PostIDs, int2String(p.ID))
				c.Mutex.Unlock()
			}

			logger.Debug(board + "/" + threadID + ": " + nint2String(len(posts)) + " posts")
			wg.Done()
		}(&wg, c.Board, threadID)
	}

	wg.Wait()

	logger.Info("Found " + nint2String(len(c.Posts)) + " Posts in total on " + c.Board)

	return nil
}
