package main

import (
	"errors"
	"testing"
)

func TestMailError(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			logger.Info("Panic was thrown")
		} else {
			logger.Info("Panic was not thrown")
			t.Fail()
		}
	}()

	err := errors.New("some error message")
	mailPanic(NewError(err))
}
