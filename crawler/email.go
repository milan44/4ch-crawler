package main

import (
	"crypto/tls"
	"runtime/debug"
	"strings"

	gomail "gopkg.in/mail.v2"
)

const (
	emailFrom     = "crawler@wiese2.org"
	emailPassword = "AtaNticUlIVErbeAforIg"
	emailTo       = "info@wiese2.org"
	emailHost     = "mail.lima-city.de"
	emailPort     = 465

	emailStyle = "p{font-family:sans-serif;margin:4px 0;font-size:14px}" +
		"pre{margin:8px 0}" +
		"code{word-break:break-word;display:inline-block;white-space:pre-wrap;line-height:17px;counter-reset:line;" +
		"color:#e83e8c;background:#fff;font-size:13px}" +
		"pre code{padding-left:32px;width:100%}" +
		"code span{position:relative}" +
		"code span:before{counter-increment:line;content:counter(line);display:inline-block;border-right:1px solid #ddd;padding:0 .5em;" +
		"height:100%;margin-right:.5em;color:#888;position:absolute;top:0;right:100%;width:28px;text-align:right}"
)

var isMailPanic = false

func mailPanic(err Error) {
	msg := "<style>" + emailStyle + "</style>" +
		"<p><b>Crawler:</b> 4chan Crawler</p>" +
		"<p><b>Error:</b> <code>" + err.Error() + "</code></p>" +
		"<p><b>Debug-Trace:</b></p><pre><code>" + spanifyLines(string(debug.Stack())) + "</code></pre>" +
		"<p><b>Call-Trace:</b></p><pre><code>" + spanifyLines(err.Trace()) + "</code></pre>"

	logger.Info("Sending Error Mail")

	mailErr := sendEmail("Crawler Error occured", msg, true)
	if mailErr != nil {
		logger.FatalE(mailErr)
	}

	isMailPanic = true
	logger.Panic(err.GetError())
}

func spanifyLines(text string) string {
	lines := strings.Split(strings.TrimSpace(text), "\n")
	for i, line := range lines {
		lines[i] = "<span>" + line + "</span>"
	}
	return strings.Join(lines, "\n")
}

func sendEmail(subject string, body string, isHTML bool) error {
	m := gomail.NewMessage()

	m.SetHeaders(map[string][]string{
		"From":    []string{emailFrom},
		"To":      []string{emailTo},
		"Subject": []string{subject},
	})

	if isHTML {
		m.SetBody("text/html", body)
	} else {
		m.SetBody("text/plain", body)
	}

	d := gomail.NewDialer(emailHost, emailPort, emailFrom, emailPassword)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	return d.DialAndSend(m)
}
