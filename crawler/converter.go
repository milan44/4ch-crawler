package main

import "strconv"

func float2String(f float64) string {
	return strconv.FormatInt(int64(f), 10)
}
func float2Int(f float64) int64 {
	return int64(f)
}
func int2String(i int64) string {
	return strconv.FormatInt(i, 10)
}
func nint2String(i int) string {
	return strconv.Itoa(i)
}

func i2String(i interface{}) string {
	if v, ok := i.(string); ok {
		return v
	}
	return ""
}
func i2Float(i interface{}) float64 {
	if v, ok := i.(float64); ok {
		return v
	}
	return 0
}
