package main

import (
	"bufio"
	"os"
	"regexp"
	"strings"
)

var rgx = regexp.MustCompile(`(?miU)<a href=.* class="quotelink">|</a>|` +
	`<span class="quote">|</span>|<span class=""deadlink"">|` +
	`<s>|</s>|<span class=""fortune"".*>.*?|` +
	`<a href=.*>|<iframe .*></iframe>|<img src=.*>|<span style=.*>|` +
	`<strong .*>|</strong>|<b>|</b>`)

func normalizeBody(text string) string {
	text = strings.ReplaceAll(text, "’", "'")
	text = strings.ReplaceAll(text, "<br>", "\n")
	text = strings.ReplaceAll(text, "<wbr>", "")
	text = strings.ReplaceAll(text, "&#039;", "'")

	text = rgx.ReplaceAllString(text, "")

	text = htmlEntityDecode(text)

	rgx = regexp.MustCompile(`(?m)\s+$`)
	text = rgx.ReplaceAllString(text, "")

	rgx = regexp.MustCompile(`(?m)[\r\n]{3,}`)
	text = rgx.ReplaceAllString(text, "\n\n")

	return strings.TrimSpace(text)
}

func runNormalizer() error {
	csvFiles, err := findAllCsvFilesInDirectory(dataDir)
	if err != nil {
		return err
	}

	err = makeBackup()
	if err != nil {
		return err
	}

	for _, csvFile := range csvFiles {
		logger.Info("Normalizing " + csvFile.Pathname + "...")

		file, err := os.Open(csvFile.Pathname)
		if err != nil {
			return err
		}
		defer file.Close()

		dest := csvFile.Pathname + "_dst"

		if _, err := os.Stat(dest); err == nil {
			_ = os.RemoveAll(dest)
		}

		dst, err := os.Create(dest)
		if err != nil {
			return err
		}
		defer dst.Close()

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			text := strings.TrimSpace(scanner.Text())

			if text == "" {
				continue
			}

			text = rgx.ReplaceAllString(text, "")

			_, _ = dst.WriteString(text + "\n")
		}

		if err := scanner.Err(); err != nil {
			return err
		}

		_ = dst.Close()
		_ = file.Close()

		err = os.RemoveAll(csvFile.Pathname)
		if err != nil {
			return err
		}

		err = os.Rename(dest, csvFile.Pathname)
		if err != nil {
			return err
		}
	}

	logger.Info("Normalizing completed")

	return nil
}

func htmlEntityDecode(html string) string {
	html = strings.ReplaceAll(html, "&gt;", ">")
	html = strings.ReplaceAll(html, "&lt;", ">")
	html = strings.ReplaceAll(html, "&quot;", "\"")

	return html
}
