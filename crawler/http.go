package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"
)

func loadJSONMap(url string, tries int) (map[string]interface{}, Error) {
	data, err := loadBytes(url)
	if err != nil {
		return nil, err
	}

	result := make(map[string]interface{})
	err2 := json.Unmarshal(data, &result)
	if err2 != nil {
		if tries >= 1 {
			logger.Warning("json.Unmarshal failed (" + url + "), skipping")
			return nil, NewError(errors.New("skip"))
		}
		logger.Warning("json.Unmarshal failed, waiting one second and trying again")

		time.Sleep(1 * time.Second)
		return loadJSONMap(url, tries+1)
	}

	return result, nil
}

func loadJSONArray(url string) ([]map[string]interface{}, Error) {
	data, err := loadBytes(url)
	if err != nil {
		return nil, err
	}

	result := make([]map[string]interface{}, 0)
	err2 := json.Unmarshal(data, &result)
	if err != nil {
		return nil, NewError(err2)
	}

	return result, nil
}

func loadBytes(url string) ([]byte, Error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, NewError(err)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, NewError(err)
	}

	return data, nil
}
