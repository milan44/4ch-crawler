package main

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"github.com/gobuffalo/packr/v2"
)

func mkdir(dir string) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		os.Mkdir(dir, 0777)
	}
}

func initFile(file string, content string) {
	if _, err := os.Stat(file); os.IsNotExist(err) {
		_ = ioutil.WriteFile(file, []byte(content), 0777)
	}
}

func loadFileJSON(file string) []string {
	b, _ := ioutil.ReadFile(file)

	var res []string
	json.Unmarshal(b, &res)

	return res
}

func writeFileJSON(file string, jsonSlice []string) {
	b, _ := json.Marshal(jsonSlice)

	_ = ioutil.WriteFile(file, b, 0777)
}

func writeFileJSONCrawlerInfo(file string, jsonSlice map[string]CrawlerInfo) {
	b, _ := json.Marshal(jsonSlice)

	_ = ioutil.WriteFile(file, b, 0777)
}

func appendToFile(file string, text string) {
	f, _ := os.OpenFile(file, os.O_APPEND|os.O_WRONLY, 0777)
	_, _ = f.WriteString(text)
	_ = f.Close()
}

func touch(fileName string) Error {
	_, err := os.Stat(fileName)
	if os.IsNotExist(err) {
		file, err := os.Create(fileName)
		if err != nil {
			return NewError(err)
		}
		defer file.Close()
	}
	return nil
}
func exists(fileName string) bool {
	_, err := os.Stat(fileName)
	return !os.IsNotExist(err)
}

func doPackrBox() Error {
	box := packr.New("Static stuff", "./data")

	data, err := box.FindString("index.html")
	if err != nil {
		return NewError(err)
	}
	_ = ioutil.WriteFile(dataDir+"index.html", []byte(data), 0777)

	mkdir(dataDir + "assets")

	data, err = box.FindString("index.js")
	if err != nil {
		return NewError(err)
	}
	_ = ioutil.WriteFile(dataDir+"assets/index.js", []byte(data), 0777)

	data, err = box.FindString("index.css")
	if err != nil {
		return NewError(err)
	}
	_ = ioutil.WriteFile(dataDir+"assets/index.css", []byte(data), 0777)

	return nil
}
