package main

import (
	"errors"
	"fmt"
	"os"

	mlogger "gitlab.com/milan44/logger"
)

var logger = mlogger.NewShortLogger()

const (
	dataDir   = "4chan" + string(os.PathSeparator)
	statsFile = dataDir + "4stats.txt"
)

var (
	boards = []string{
		"b",
		"r9k",
		"sci",
		"s4s",
		"int",
		"lit",
		"k",
		"x",
		"a",
		"v",
		"n",
		"vg",
		"v",
		"his",
		"adv",

		"an",
		"biz",
		"ck",
		"fa",
		"fit",
		"ic",
		"p",
		"tv",
	}
)

func main() {
	defer func() {
		if r := recover(); r != nil {
			if !isMailPanic {
				logger.Info("Caught unexpected panic")
				mailPanic(NewError(errors.New(fmt.Sprint("Recovered: ", r))))
			}
		}
	}()

	args := os.Args[1:]

	runCrawler := true
	runInfo := true
	runGraph := true

	if len(args) > 0 {
		if has(args, "normal") {
			err := runNormalizer()
			if err != nil {
				panic(err)
			}
			return
		}

		if has(args, "graph") || has(args, "info") {
			runCrawler = false
			runGraph = false
			runInfo = false
		}

		if has(args, "graph") {
			runGraph = true
		}
		if has(args, "info") {
			runInfo = true
		}
	}

	if runCrawler {
		c := ChanCrawler{}
		err := c.crawl(boards)
		if err != nil {
			mailPanic(err)
		}
	}

	if runInfo {
		logger.Info("Updating info data")
		err := postInfo()
		if err != nil {
			mailPanic(err)
		}
	}

	if runGraph {
		logger.Info("Rebuilding info graph")
		err := renderGraph()
		if err != nil {
			mailPanic(err)
		}
	}

	logger.Info("Updating static files")
	err := doPackrBox()
	if err != nil {
		mailPanic(err)
	}

	logger.Info("Done")
}

func has(sl []string, s string) bool {
	for _, ss := range sl {
		if ss == s {
			return true
		}
	}
	return false
}
