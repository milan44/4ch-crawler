package main

import (
	"bytes"
	"bufio"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type CrawlerInfo struct {
	DirSize        int64
	PostCount      int
	CompressedSize int64
	FirstCrawl     *string
}

type AdvancedFileInfo struct {
	FileInfo os.FileInfo
	Pathname string
}

func postInfo() Error {
	initFile(statsFile, "Post Count|Dir Size|Total added Posts|Compressed Size|Date")

	info, err := collectInfo()
	if err != nil {
		return err
	}

	if info.DirSize > (3*1024*1024*1024) && !exists(dataDir+"mail_sent") {
		sendEmail("Crawler Data", "The total crawled data is bigger than 3GB.", false)
		touch(dataDir + "mail_sent")
	}

	appendToFile(statsFile, "\n"+info.Format())

	initFile(dataDir+"initial_write", int2String(time.Now().Unix()))

	writeFileJSONCrawlerInfo(dataDir+"index.json", fileSizeMap)

	return nil
}

var fileSizeMap = map[string]CrawlerInfo{}

func collectInfo() (CrawlerInfo, Error) {
	csvFiles, err := findAllCsvFilesInDirectory(dataDir)
	if err != nil {
		return CrawlerInfo{}, err
	}

	crawlerInfo := CrawlerInfo{}
	for _, csvFile := range csvFiles {
		crawlerInfo.DirSize += csvFile.FileInfo.Size()

		f, err := os.Open(csvFile.Pathname)
		if err != nil {
			return CrawlerInfo{}, NewError(err)
		}

		postCount, err2 := lineCounter(f)
		if err != nil {
			return CrawlerInfo{}, err2
		}

		crawlerInfo.PostCount += postCount

		fileSizeMap[csvFile.Pathname] = CrawlerInfo{
			DirSize:   csvFile.FileInfo.Size(),
			PostCount: postCount,
			FirstCrawl: getFirstCrawl(csvFile.Pathname),
		}
	}

	compressedSize, err := calculateCompressedSize()
	if err != nil {
		return CrawlerInfo{}, err
	}

	crawlerInfo.CompressedSize = compressedSize

	return crawlerInfo, nil
}

func getFirstCrawl(csvFile string) *string {
	file, err := os.Open(csvFile)
    if err != nil {
        return nil
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
	x := 0
	dat := ""
    for scanner.Scan() && x < 2 {
        dat = strings.Split(scanner.Text(), ",")[0]
		x++
    }

    if err := scanner.Err(); err != nil {
        return nil
    }
	
	return &dat
}

func (c CrawlerInfo) Format() string {
	t := time.Now()
	zone, _ := t.Zone()

	date := time.Now().Format("02.01.2006 - 15:04") + " " + zone
	return nint2String(c.PostCount) + "|" +
		int2String(c.DirSize/1024) + "|" +
		nint2String(totalAddedPosts) + "|" +
		int2String(c.CompressedSize/1024) + "|" +
		date
}

func findAllCsvFilesInDirectory(directory string) ([]AdvancedFileInfo, Error) {
	rawFiles, err := ioutil.ReadDir(directory)
	if err != nil {
		return nil, NewError(err)
	}

	files := make([]AdvancedFileInfo, 0)

	for _, file := range rawFiles {
		filename := file.Name()

		if file.IsDir() {
			lowerFiles, err := findAllCsvFilesInDirectory(directory + "/" + filename)
			if err != nil {
				return nil, err
			}
			files = append(files, lowerFiles...)
		} else if strings.HasSuffix(filename, ".csv") {
			files = append(files, AdvancedFileInfo{
				FileInfo: file,
				Pathname: filepath.Join(directory, filename),
			})
		}
	}

	return files, nil
}

func lineCounter(r io.Reader) (int, Error) {
	buf := make([]byte, 32*1024)
	count := 0
	lineSep := []byte{'\n'}

	for {
		c, err := r.Read(buf)
		count += bytes.Count(buf[:c], lineSep)

		switch {
		case err == io.EOF:
			return count, nil

		case err != nil:
			return count, NewError(err)
		}
	}
}
