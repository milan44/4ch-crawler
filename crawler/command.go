package main

import (
	"bytes"
	"errors"
	"io"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func makeBackup() Error {
	dir := strings.TrimRight(dataDir, string(os.PathSeparator))
	logger.Info("Creating backup")

	c := exec.Command("tar", "-zcvf", "backup.tar.gz", dir)

	err := c.Run()
	if err != nil {
		return NewError(err)
	}
	return nil
}

func calculateCompressedSize() (int64, Error) {
	dir := strings.TrimRight(dataDir, string(os.PathSeparator))

	logger.Info("Calculating compressed directory size")
	logger.Debug("tar -czf - " + dir + " | wc -c")

	_, err := exec.LookPath("wc")
	if err != nil {
		return 0, NewError(err)
	}

	c1 := exec.Command("tar", "-czf", "-", dir)
	c2 := exec.Command("wc", "-c")

	reader, writer := io.Pipe()
	c1.Stdout = writer
	c2.Stdin = reader

	var b2 bytes.Buffer
	c2.Stdout = &b2

	var bErr bytes.Buffer
	c1.Stderr = &bErr

	var b2Err bytes.Buffer
	c2.Stderr = &b2Err

	err = c1.Start()
	if err != nil {
		return 0, NewError(err)
	}

	err = c2.Start()
	if err != nil {
		return 0, NewError(err)
	}

	err = c1.Wait()
	if err != nil {
		return 0, NewError(errors.New(string(bErr.Bytes())))
	}

	writer.Close()

	err = c2.Wait()
	if err != nil {
		return 0, NewError(errors.New(string(b2Err.Bytes())))
	}

	out := string(b2.Bytes())

	i, err := strconv.ParseInt(strings.TrimSpace(out), 10, 64)
	return i, NewError(err)
}
