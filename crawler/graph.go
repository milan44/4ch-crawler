package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"

	chart "github.com/wcharczuk/go-chart"
	"github.com/wcharczuk/go-chart/drawing"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

type GraphData struct {
	YAxis1 []float64
	YAxis2 []float64
	YAxis3 []float64
	XAxis  []float64
	Labels []chart.Tick
}

func renderGraph() Error {
	data, err := loadGraphData()
	if err != nil {
		return err
	}

	graph := chart.Chart{
		Title: "Crawler Statistics",
		TitleStyle: chart.Style{
			Show:        true,
			StrokeColor: drawing.ColorFromHex("586e75"),
		},
		Width:  2560,
		Height: 1080,
		Background: chart.Style{
			FillColor: drawing.ColorFromHex("fdf6e3"),
		},
		Canvas: chart.Style{
			FillColor: drawing.ColorFromHex("fdf6e3"),
		},
		XAxis: chart.XAxis{
			Style: chart.Style{
				Show:        true,
				StrokeColor: drawing.ColorFromHex("586e75"),
			},
			Ticks: data.Labels,
		},
		YAxis: chart.YAxis{
			Style: chart.Style{
				Show:      true,
				FillColor: drawing.ColorFromHex("586e75"),
			},
			ValueFormatter: func(v interface{}) string {
				if vf, isFloat := v.(float64); isFloat {
					p := message.NewPrinter(language.English)
					vi := int64(vf)
					return p.Sprintf("%d", vi) + "p / " + ByteCountIEC(vi*1024)
				}
				return ""
			},
		},
		Series: []chart.Series{
			chart.ContinuousSeries{
				XValues: data.XAxis,
				YValues: data.YAxis1,
				Style: chart.Style{
					Show:        true,
					StrokeColor: drawing.ColorFromHex("dc322f"),
					FillColor:   drawing.ColorFromHex("dc322f").WithAlpha(25),
				},
			},
			chart.ContinuousSeries{
				XValues: data.XAxis,
				YValues: data.YAxis2,
				Style: chart.Style{
					Show:        true,
					StrokeColor: drawing.ColorFromHex("268bd2"),
					FillColor:   drawing.ColorFromHex("268bd2").WithAlpha(25),
				},
			},
			chart.ContinuousSeries{
				XValues: data.XAxis,
				YValues: data.YAxis3,
				Style: chart.Style{
					Show:        true,
					StrokeColor: drawing.ColorFromHex("859900"),
					FillColor:   drawing.ColorFromHex("859900").WithAlpha(25),
				},
			},
		},
	}

	f, _ := os.Create(dataDir + "4chan.png")
	defer f.Close()
	return NewError(graph.Render(chart.PNG, f))
}

func loadGraphData() (GraphData, Error) {
	file1, err := os.Open(statsFile)
	if err != nil {
		return GraphData{}, NewError(err)
	}
	defer file1.Close()

	intLines, err2 := lineCounter(file1)
	if err != nil {
		return GraphData{}, err2
	}

	lines := float64(intLines)
	modulus := math.Floor(lines / 22)

	file, err := os.Open(statsFile)
	if err != nil {
		return GraphData{}, NewError(err)
	}
	defer file.Close()

	data := GraphData{
		YAxis1: make([]float64, 0),
		YAxis2: make([]float64, 0),
		XAxis:  make([]float64, 0),
		Labels: make([]chart.Tick, 0),
	}

	index := 0.0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		lineData := strings.Split(line, "|")

		if len(lineData) != 5 || index == 0.0 {
			index++
			continue
		}

		if index == 1.0 {
			// Board directory with initialized .csv and .json
			// files is roughly 8192 bytes big, plus initialized
			// 4stats.txt which is roughly 4096 bytes big
			emptyFileSize := math.Floor(float64((8192*len(boards))+4096) / 1024)

			// 0.39 is the estmiated compression ration
			emptyCompressedSize := math.Floor(emptyFileSize * 0.39)

			data.YAxis1 = append(data.YAxis1, emptyFileSize)
			data.YAxis2 = append(data.YAxis2, 0.0)
			data.YAxis3 = append(data.YAxis3, emptyCompressedSize)
			data.XAxis = append(data.XAxis, 0.0)
			data.Labels = append(data.Labels, chart.Tick{
				Value: 0.0,
				Label: strings.Split(lineData[4], " ")[0],
			})
		}

		postCount, err := strconv.Atoi(lineData[0])
		if err != nil {
			return GraphData{}, NewError(err)
		}

		dirSize, err := strconv.Atoi(lineData[1])
		if err != nil {
			return GraphData{}, NewError(err)
		}

		compressedSize, err := strconv.Atoi(lineData[3])
		if err != nil {
			return GraphData{}, NewError(err)
		}

		if math.Mod(index, modulus) == 0 {
			lineData[4] = strings.Split(lineData[4], " ")[0]
		} else {
			lineData[4] = ""
		}

		data.YAxis1 = append(data.YAxis1, float64(dirSize))
		data.YAxis2 = append(data.YAxis2, float64(postCount))
		data.YAxis3 = append(data.YAxis3, float64(compressedSize))
		data.XAxis = append(data.XAxis, index)
		data.Labels = append(data.Labels, chart.Tick{
			Value: index,
			Label: lineData[4],
		})

		index++
	}

	if err := scanner.Err(); err != nil {
		return GraphData{}, NewError(err)
	}

	return data, nil
}

func ByteCountIEC(b int64) string {
	const unit = 1024
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.1f%ciB",
		float64(b)/float64(div), "KMGTPE"[exp])
}
