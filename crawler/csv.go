package main

import (
	"regexp"
	"strings"
)

func encodeCSVString(str string) string {
	// Standard escape characters
	str = strings.ReplaceAll(str, "\"", "\"\"")

	// Optional ones for better readibility
	rgx := regexp.MustCompile(`(?m)[\r\n]`)
	str = rgx.ReplaceAllString(str, "\\n")

	// Wrap in quotes
	return "\"" + str + "\""
}

func encodeCSVSlice(slice [][]string) string {
	text := make([]string, len(slice))

	for i, sl := range slice {
		line := make([]string, len(sl))

		for j, s := range sl {
			if strings.ContainsAny(s, "\"\n,") {
				s = encodeCSVString(s)
			}
			line[j] = s
		}

		text[i] = strings.Join(line, ",")
	}

	return strings.Join(text, "\n")
}
