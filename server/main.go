package main

import (
	"net/http"
	"os"
)

func main() {
	if _, err := os.Stat("./4chan"); os.IsNotExist(err) {
		_ = os.Mkdir("./4chan", 0777)
	}

	http.Handle("/", http.FileServer(http.Dir("./4chan")))

	err := http.ListenAndServe(":6262", nil)
	if err != nil {
		panic(err)
	}
}
