package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	mlogger "gitlab.com/milan44/logger"
)

var logger = mlogger.NewShortLogger()

const (
	dataDir = "4chan" + slash + "tar-gz" + slash
	chanDir = "4chan" + slash
	slash   = string(os.PathSeparator)
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Are you sure you want to archive? [no/yes] ")
	text, _ := reader.ReadString('\n')
	if strings.TrimSpace(strings.ToLower(text)) != "yes" {
		logger.Warning("Aborting archive")
		return
	}

	logger.Info("Preparing directories")
	prepareDirectories()

	logger.Info("Loading csv files")
	csvFiles, err := collectCsvFiles(chanDir)
	if err != nil {
		logger.Panic(err)
	}

	logger.Info("Copying csv files")
	for _, csvFile := range csvFiles {
		path := strings.Split(csvFile, slash)
		board := path[len(path)-2]

		logger.Debug("Moving " + board + ".csv")

		err := os.Rename(csvFile, dataDir+board+".csv")
		if err != nil {
			logger.Panic(err)
		}
	}

	logger.Info("Creating tar archive")

	tar, err := getTarName()
	if err != nil {
		logger.Panic(err)
	}

	err = runCommand("tar", []string{
		"--totals=USR1",
		"-czf",
		chanDir + tar,
		strings.TrimRight(dataDir, slash),
	})
	if err != nil {
		logger.Panic(err)
	}

	logger.Info("Cleaning up")
	err = os.RemoveAll(dataDir)
	if err != nil {
		logger.Panic(err)
	}
	err = os.Remove(chanDir + "initial_write")
	if err != nil {
		logger.Panic(err)
	}

	logger.Info("Done")
}

func collectCsvFiles(dir string) ([]string, error) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	result := make([]string, 0)
	for _, file := range files {
		filename := dir + file.Name()

		if file.IsDir() {
			f, err := collectCsvFiles(filename + slash)
			if err != nil {
				return nil, err
			}

			result = append(result, f...)
		} else if strings.HasSuffix(filename, ".csv") {
			result = append(result, filename)
		}
	}

	return result, nil
}
