package main

import (
	"os"
	"os/exec"
)

func runCommand(command string, args []string) error {
	path, err := exec.LookPath(command)
	if err != nil {
		return err
	}
	cmd := exec.Command(path, args...)
	cmd.Stdout = os.Stdout
	return cmd.Run()
}
