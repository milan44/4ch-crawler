package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"time"
)

func mkdir(dir string) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		os.Mkdir(dir, 0777)
	}
}

func prepareDirectories() {
	mkdir("4chan")
	if _, err := os.Stat(dataDir); !os.IsNotExist(err) {
		err := os.RemoveAll(dataDir)
		if err != nil {
			logger.Panic(err)
		}
	}
	mkdir(dataDir)
}

func copy(src, dst string) error {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer destination.Close()
	_, err = io.Copy(destination, source)
	return err
}

func getTarName() (string, error) {
	body, err := ioutil.ReadFile(chanDir + "initial_write")
	if err != nil {
		return "", err
	}

	i, err := strconv.Atoi(string(body))
	if err != nil {
		return "", err
	}

	return formatDate(timestampToDate(i)) + "__" + formatDate(time.Now()) + ".tar.gz", nil
}
