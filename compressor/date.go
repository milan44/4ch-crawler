package main

import "time"

func formatDate(date time.Time) string {
	return date.Format("02-01-2006_15-04-05")
}

func timestampToDate(timestamp int) time.Time {
	return time.Unix(int64(timestamp), 0)
}
