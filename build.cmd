@echo off
echo Preparing environment...

set GOARCH=amd64
set GOOS=linux
::go install -v -a std

echo Building crawler...
cd crawler
packr2 build -o ..\build\crawler

echo Building compressor...
cd ..\compressor
go build -o ..\build\compressor

echo Building server...
cd ..\server
go build -o ..\build\server
cd ..

echo Done